<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SendPushNotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sendPushNotification/{token}', [SendPushNotificationController::class, 'sendPushNotification'])->name('send-push-notification');

Route::get('/temporaryPage', [SendPushNotificationController::class, 'temporaryPage'])->name('temporary-page');