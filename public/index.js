if('serviceWorker' in navigator) {
    window.addEventListener("load", function() {
        navigator.serviceWorker.register('./firebase-messaging-sw.js')
            .then(reg => console.log("service worker registered ", reg))
            .catch(error => console.log(`Service worker registration error : ${error}`));
    });
}