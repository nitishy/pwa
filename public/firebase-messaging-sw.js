importScripts("https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.9/firebase-analytics.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.9/firebase-messaging.js");


var firebaseConfig = {
    apiKey: "AIzaSyC-MOAjiTOpJ55Zh6HsoHJ3o1EXMy8X1Is",
    authDomain: "push-notifocations-test-87087.firebaseapp.com",
    projectId: "push-notifocations-test-87087",
    storageBucket: "push-notifocations-test-87087.appspot.com",
    messagingSenderId: "498712842926",
    appId: "1:498712842926:web:445a1de08bf5deda1ad33f",
    measurementId: "G-0HPJD1B1S3"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const messaging2 = firebase.messaging();

messaging2.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  const notificationOptions = {
    //sender:payload.data.sender,
    body: payload.notification.body,
    click_action: payload.notification.click_action,
  };
  console.log('Background notification');
  return self.registration.showNotification(payload.notification.title,
      notificationOptions);
});

self.addEventListener('fetch', function(event) {});
  