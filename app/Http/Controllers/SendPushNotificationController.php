<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SendPushNotificationController extends Controller
{
    public function __construct()
    {
        $this->serverKey = config('app.firebase_server_key');
    }

    public function sendPushNotification($push_token)
    {
        $data = [
            "to" => $push_token,
            "notification" => [
                "title" => 'Sample Web Push Notification',
                "body" => "Hi! How are you?",
                "click_action" => "http://127.0.0.1:8000/temporaryPage",
            ],
            "data" => [
                "sender"=> "Nithish",
            ]             
        ];
        $dataString = json_encode($data);
        $headers = [
            'Authorization: key=' . $this->serverKey,
            'Content-Type: application/json',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_exec($ch);

        return new Response("Notification Sent");
    }

    public function temporaryPage()
    {
        return view("temporary-page");
    }
}
